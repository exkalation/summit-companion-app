/**
 * Welcome to your Workbox-powered service worker!
 *
 * You'll need to register this file in your web app and you should
 * disable HTTP caching for this file too.
 * See https://goo.gl/nhQhGp
 *
 * The rest of the code is auto-generated. Please don't update this file
 * directly; instead, make changes to your Workbox build configuration
 * and re-run your build process.
 * See https://goo.gl/2aRDsh
 */

importScripts("https://storage.googleapis.com/workbox-cdn/releases/4.3.1/workbox-sw.js");

self.addEventListener('message', (event) => {
  if (event.data && event.data.type === 'SKIP_WAITING') {
    self.skipWaiting();
  }
});

/**
 * The workboxSW.precacheAndRoute() method efficiently caches and responds to
 * requests for URLs in the manifest.
 * See https://goo.gl/S9QRab
 */
self.__precacheManifest = [
  {
    "url": "api/categoryMap.json",
    "revision": "c58c87b6b83541dc82f82a81f83a8cf9"
  },
  {
    "url": "api/schedule.json",
    "revision": "233a22f075b40ca9c870adcd8b1d454f"
  },
  {
    "url": "assets/icons/apple-touch-icon.png",
    "revision": "642166be1556f4fe854ead8bf6ff6406"
  },
  {
    "url": "assets/icons/favicon-16x16.png",
    "revision": "5f4f43b5153e47e5a646d8abe31a4017"
  },
  {
    "url": "assets/icons/favicon-32x32.png",
    "revision": "15dedaca53cbd970e0e38d6942feda57"
  },
  {
    "url": "assets/icons/favicon.ico",
    "revision": "286da9ac4563121e664e6bde20857b67"
  },
  {
    "url": "assets/icons/summit-companion-app_icon144.png",
    "revision": "b515a82d396e58aa97cbadaf8994c276"
  },
  {
    "url": "assets/icons/summit-companion-app_icon168.png",
    "revision": "e690fbd685b3b92216e8582649adf1be"
  },
  {
    "url": "assets/icons/summit-companion-app_icon192.png",
    "revision": "4c3a05bd62f453b8341f7980f72707c4"
  },
  {
    "url": "assets/icons/summit-companion-app_icon436.png",
    "revision": "5360391f7f022469ad308987b33eae11"
  },
  {
    "url": "assets/icons/summit-companion-app_icon48.png",
    "revision": "0da3da7931fed1b8a19ac8729c4cf5c8"
  },
  {
    "url": "assets/icons/summit-companion-app_icon72.png",
    "revision": "2f989dfd6289615951cb4f471ac67a0d"
  },
  {
    "url": "assets/icons/summit-companion-app_icon96.png",
    "revision": "681f32dcc6de72b5a808b0e1c3774520"
  },
  {
    "url": "assets/lib/bootstrap.min.css",
    "revision": "d7bc39028fd0d81fb210be49ecb114ad"
  },
  {
    "url": "assets/lib/material-icons.flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2",
    "revision": "d7e60f9d1433a45ed71817f6d23abeca"
  },
  {
    "url": "assets/styles.css",
    "revision": "e2a151ebebb94d0db79b086f2d7a5554"
  },
  {
    "url": "companion.min.js",
    "revision": "19bfc4274f7e155f03dbd4c7fb591258"
  },
  {
    "url": "index.html",
    "revision": "9d8b17d7c3f40de97e98d74e2a6f16dc"
  }
].concat(self.__precacheManifest || []);
workbox.precaching.precacheAndRoute(self.__precacheManifest, {});
