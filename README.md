# summit-companion-app

## How to update the schedule
For example, for the 2019 Adobe Summit EMEA:
1. Go to: https://summit-emea.adobe.com/emea/sessions/schedule
2. Run:
`JSON.stringify($('div>span.sessionTitle').get().map(e => {
    const elem = $(e);
    const id = elem.find('strong').attr('data-target').replace('#', '');
    const dateTimeContainer = elem.parent().parent().parent().parent();
    return { 'id': id
      , 'title': elem.text()
      , 'category': elem.text().replace(/([^ ]+).*/g, '$1').replace(/[0-9]+$/, '')
      , 'room': elem.parent().find('p').first().text()
      , 'date': dateTimeContainer.find('div>h5').text().replace(/([a-z]+) ([0-9]+) ([a-z]+)/gi, '$3 $2, $1')
      , 'time': dateTimeContainer.find('div>h2').text()
      , 'detailsURL': $('#' + id).find('div>a').attr('href') };
  }));`
3. Check that for duplicate IDs by comparing the length of the array resulting from this:
`dataFromPreviousStep.reduce((acc, item) => { if(!acc.some(i => item.id === i)){ acc.push(item.id); } return acc; }, []).length`

## TODOs
1. Generate service worker (at least pre-cache config) with build instead of manually.
