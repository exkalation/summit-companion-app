module.exports = {
  "globDirectory": ".",
  "globPatterns": [
    "{index.html,companion.min.js,assets/**/*,api/categoryMap.json,api/schedule.json}"
  ],
  "globIgnores": [ 'assets/demo/**' ],
  "swDest": "sw.js"
};
