module Category exposing (Category, CategoryMap, categoryMapDecoder)

import Dict exposing (Dict)
import Json.Decode as Decode exposing (Decoder, string)
import Json.Decode.Pipeline exposing (optional, required)


type alias Category =
    { color : String }


type alias CategoryMap =
    Dict String Category


categoryMapDecoder : Decoder CategoryMap
categoryMapDecoder =
    Decode.dict categoryDecoder


categoryDecoder : Decoder Category
categoryDecoder =
    Decode.succeed Category
        |> required "color" string
