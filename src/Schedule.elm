module Schedule exposing (Date, DaySchedule, Schedule, Session, SessionList, Time, scheduleDecoder, toSchedule)

import Dict exposing (Dict)
import Json.Decode as Decode exposing (Decoder, string)
import Json.Decode.Pipeline exposing (optional, required)


type alias Session =
    { id : String, title : String, category : String, date : String, time : String, room : String, detailsURL : Maybe String }


type alias SessionList =
    List Session


type alias Schedule =
    Dict Date DaySchedule


type alias DaySchedule =
    Dict Time SessionList


type alias Date =
    String


type alias Time =
    String


toSchedule : SessionList -> Schedule
toSchedule list =
    List.foldl
        handleSessionItem
        Dict.empty
        list


handleSessionItem : Session -> Schedule -> Schedule
handleSessionItem session schedule =
    let
        key =
            session.date
    in
    case Dict.get key schedule of
        Just daySchedule ->
            Dict.insert key (handleSessionItemDate session daySchedule) schedule

        Nothing ->
            Dict.insert key (Dict.fromList [ ( session.time, [ session ] ) ]) schedule


handleSessionItemDate : Session -> DaySchedule -> DaySchedule
handleSessionItemDate session daySchedule =
    let
        key =
            session.time
    in
    case Dict.get key daySchedule of
        Just sessionList ->
            Dict.insert key (session :: sessionList) daySchedule

        Nothing ->
            Dict.insert key [ session ] daySchedule


scheduleDecoder : Decoder SessionList
scheduleDecoder =
    Decode.list sessionDecoder


sessionDecoder : Decoder Session
sessionDecoder =
    Decode.succeed Session
        |> required "id" string
        |> required "title" string
        |> required "category" string
        |> required "date" string
        |> required "time" string
        |> required "room" string
        |> optional "detailsURL" (Decode.map Just string) Nothing
