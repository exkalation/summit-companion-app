port module SummitCompanion exposing (main, storeData)

import Bootstrap.Accordion as Accordion
import Bootstrap.Card as Card
import Bootstrap.Card.Block as Block
import Bootstrap.Grid as Grid
import Bootstrap.Grid.Col as GridCol
import Bootstrap.Navbar as Navbar
import Browser
import Browser.Navigation as Nav
import Category exposing (Category, CategoryMap)
import Dict exposing (Dict)
import Html exposing (Html)
import Html.Attributes as HtmlA
import Html.Events as HtmlE
import Http
import MultiSelectButtonGroup as MSBG
import Schedule
import Url exposing (Url)


type alias Model =
    { config : Config
    , categoryMap : CategoryData
    , schedule : ScheduleData
    , selections : SelectionList
    , navbarState : Navbar.State
    , accordionState : Accordion.State
    , msbgFilterState : MSBG.State
    , persistenceHackInput : String
    }


type alias Config =
    { initialSelection : String
    , apiEndpoints : { schedule : String, categoryMap : String }
    }


type CategoryData
    = CategoryMapLoading
    | CategoryMapLoaded CategoryMap
    | CategoryMapLoadingFailed String


type ScheduleData
    = ScheduleLoading
    | ScheduleLoaded Schedule.Schedule
    | ScheduleLoadingFailed String


type Msg
    = RouteUrlRequest Browser.UrlRequest
    | RouteUrlChange Url
    | BookmarkSession String
    | PlanSession String
    | RemoveBookmark String
    | SetCategoryMap (Result Http.Error CategoryMap)
    | SetSchedule (Result Http.Error Schedule.SessionList)
    | AccordionMsg Accordion.State
    | SetNavbarState Navbar.State
    | SetMsbgState MSBG.State
    | PersistenceHackInput String
    | PersistenceHackStore


type alias SelectionList =
    -- previously: { title : String, date : String, time : String }
    List Selection


type Selection
    = Bookmark String
    | Plan String


port storeData : String -> Cmd msg


main : Program Config Model Msg
main =
    Browser.application
        { init = init
        , onUrlChange = RouteUrlChange
        , onUrlRequest = RouteUrlRequest
        , subscriptions = subscriptions
        , update = update
        , view = view
        }


init : Config -> Url -> Nav.Key -> ( Model, Cmd Msg )
init config url navKey =
    let
        ( navbarState, navbarCmd ) =
            Navbar.initialState SetNavbarState

        model =
            { -- Global
              config = config
            , categoryMap = CategoryMapLoading
            , schedule = ScheduleLoading
            , selections = fromStorableString config.initialSelection
            , navbarState = navbarState
            , accordionState = Accordion.initialState
            , msbgFilterState = MSBG.initialState
            , persistenceHackInput = ""
            }
    in
    ( model
    , Cmd.batch
        [ navbarCmd
        , loadCategoryMap model.config.apiEndpoints.categoryMap
        , loadSchedule model.config.apiEndpoints.schedule
        ]
    )



-- UPDATE


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        SetCategoryMap result ->
            case result of
                Err httpError ->
                    ( { model | categoryMap = CategoryMapLoadingFailed <| getErrorMessageFromHttpError httpError }
                    , Cmd.none
                    )

                Ok categoryMap ->
                    ( { model | categoryMap = CategoryMapLoaded categoryMap }, Cmd.none )

        SetSchedule result ->
            case result of
                Err httpError ->
                    ( { model | schedule = ScheduleLoadingFailed <| getErrorMessageFromHttpError httpError }
                    , Cmd.none
                    )

                Ok sessionList ->
                    ( { model | schedule = ScheduleLoaded <| Schedule.toSchedule sessionList }, Cmd.none )

        SetNavbarState state ->
            ( { model | navbarState = state }, Cmd.none )

        AccordionMsg state ->
            ( { model | accordionState = state }, Cmd.none )

        SetMsbgState state ->
            ( { model | msbgFilterState = state }, Cmd.none )

        BookmarkSession sessionId ->
            { model | selections = updateSelections model.selections (Bookmark sessionId) }
                |> handleUpdate

        PlanSession sessionId ->
            { model | selections = updateSelections model.selections (Plan sessionId) }
                |> handleUpdate

        PersistenceHackInput str ->
            ( { model | persistenceHackInput = str }, Cmd.none )

        PersistenceHackStore ->
            { model
                | selections =
                    if String.length model.persistenceHackInput >= 3 then
                        if model.persistenceHackInput == "DEL" then
                            fromStorableString ""

                        else
                            fromStorableString model.persistenceHackInput

                    else
                        model.selections
            }
                |> handleUpdate

        _ ->
            ( model, Cmd.none )


updateSelections selections selection =
    let
        cleaned =
            removeSelection selections selection
    in
    if List.any ((==) selection) selections then
        cleaned

    else
        selection :: cleaned


removeSelection selections selection =
    let
        sessionId =
            case selection of
                Bookmark id ->
                    id

                Plan id ->
                    id
    in
    List.filter ((/=) (Bookmark sessionId)) selections
        |> List.filter ((/=) (Plan sessionId))


fromStorableString : String -> SelectionList
fromStorableString selections =
    String.split "," selections
        |> List.map
            (\i ->
                if String.startsWith "!" i then
                    Plan <| String.dropLeft 1 i

                else
                    Bookmark i
            )


toStorableString : SelectionList -> String
toStorableString selections =
    selections
        |> List.map
            (\i ->
                case i of
                    Bookmark id ->
                        id

                    Plan id ->
                        "!" ++ id
            )
        |> String.join ","


handleUpdate : Model -> ( Model, Cmd Msg )
handleUpdate newModel =
    ( newModel
    , storeData (toStorableString newModel.selections)
    )


loadCategoryMap : String -> Cmd Msg
loadCategoryMap endpoint =
    Http.get
        { url = endpoint
        , expect = Http.expectJson SetCategoryMap Category.categoryMapDecoder
        }


loadSchedule : String -> Cmd Msg
loadSchedule endpoint =
    Http.get
        { url = endpoint
        , expect = Http.expectJson SetSchedule Schedule.scheduleDecoder
        }


getErrorMessageFromHttpError : Http.Error -> String
getErrorMessageFromHttpError httpError =
    case httpError of
        Http.BadBody m ->
            m

        _ ->
            "Error while loading data. (Related to request URL, response code or network)"



-- SUBSCRIPTIONS


subscriptions : Model -> Sub Msg
subscriptions model =
    Accordion.subscriptions model.accordionState AccordionMsg



-- VIEW


view : Model -> Browser.Document Msg
view model =
    { title = "Summit Companion App"
    , body =
        [ Grid.containerFluid [ HtmlA.class "h-100" ]
            [ renderNavBar model
            , renderFilters model
            , Html.main_ [] [ renderSessions model ]
            ]
        ]
    }


renderNavBar : Model -> Html.Html Msg
renderNavBar model =
    Navbar.config SetNavbarState
        |> Navbar.withAnimation
        |> Navbar.lightCustomClass "navbar-light"
        |> Navbar.collapseSmall
        |> Navbar.brand []
            [ -- Html.img [ HtmlA.id "logo", HtmlA.src "assets/..." ] []
              Html.text "Summit Companion App"
            ]
        |> Navbar.view model.navbarState


renderFilters : Model -> Html.Html Msg
renderFilters model =
    Html.div [ HtmlA.class "filters" ]
        [ MSBG.config "selection" "All" SetMsbgState
            |> MSBG.buttons
                [ ( "bookmarked", "Bookmarked" )
                , ( "planned", "Planned" )
                ]
            |> MSBG.view [] model.msbgFilterState
        ]


renderSessions : Model -> Html.Html Msg
renderSessions model =
    case ( model.categoryMap, model.schedule ) of
        ( CategoryMapLoading, _ ) ->
            renderLoading

        ( _, ScheduleLoading ) ->
            renderLoading

        ( CategoryMapLoaded categoryMap, ScheduleLoaded schedule ) ->
            Accordion.config AccordionMsg
                |> Accordion.withAnimation
                |> Accordion.cards
                    (List.append
                        (Dict.map (renderScheduleDay categoryMap model.msbgFilterState model.selections) schedule |> Dict.values)
                        [ renderHacks model ]
                    )
                |> Accordion.view model.accordionState

        ( CategoryMapLoadingFailed err, _ ) ->
            Html.text ("Error loading category map: " ++ err)

        ( _, ScheduleLoadingFailed err ) ->
            Html.text ("Error loading schedule: " ++ err)


renderLoading : Html.Html Msg
renderLoading =
    Html.div
        [ HtmlA.class "d-flex", HtmlA.class "m-5", HtmlA.class "justify-content-center" ]
        [ Html.div
            [ HtmlA.class "spinner-border", HtmlA.class "text-primary", HtmlA.attribute "role" "status" ]
            [ Html.span
                [ HtmlA.class "sr-only" ]
                [ Html.text "Loading schedule..." ]
            ]
        ]


renderScheduleDay : CategoryMap -> MSBG.State -> SelectionList -> Schedule.Date -> Schedule.DaySchedule -> Accordion.Card Msg
renderScheduleDay categoryMap filterState selections date daySchedule =
    Accordion.card
        { id = date
        , options = []
        , header =
            Accordion.headerH3 [] <| Accordion.toggle [] [ Html.text date ]
        , blocks =
            [ Accordion.block [ Block.attrs [ HtmlA.class "sessions" ] ]
                --(Grid.containerFluid [] [ Grid.simpleRow <| List.map (renderSessionV2 categoryMap) filteredSessions ])
                (daySchedule
                    |> Dict.map (renderScheduleDateTime categoryMap filterState selections date)
                    |> Dict.values
                )
            ]
        }


renderScheduleDateTime : CategoryMap -> MSBG.State -> SelectionList -> Schedule.Date -> Schedule.Time -> Schedule.SessionList -> Block.Item Msg
renderScheduleDateTime categoryMap filterState selections date time sessionList =
    let
        filteredSessions =
            List.filter (isSessionFiltered filterState selections) sessionList
    in
    Block.custom
        (Html.div
            []
            [ Html.h4 [] [ Html.text (time ++ " (showing " ++ (String.fromInt << List.length) filteredSessions ++ " of " ++ (String.fromInt << List.length) sessionList ++ ")") ]
            , Card.columns <| List.map (renderSession categoryMap selections) filteredSessions
            ]
        )


isSessionFiltered : MSBG.State -> SelectionList -> Schedule.Session -> Bool
isSessionFiltered filterState selections session =
    let
        isBookmarked =
            List.any ((==) (Bookmark session.id)) selections

        isPlanned =
            List.any ((==) (Plan session.id)) selections
    in
    if MSBG.isAnySelected "selection" filterState then
        True

    else if isBookmarked && MSBG.isOptionSelected "bookmarked" "selection" filterState then
        True

    else if isPlanned && MSBG.isOptionSelected "planned" "selection" filterState then
        True

    else
        False


renderSession : CategoryMap -> SelectionList -> Schedule.Session -> Card.Config Msg
renderSession categoryMap selections session =
    let
        isBookmarked =
            List.any ((==) (Bookmark session.id)) selections

        isPlanned =
            List.any ((==) (Plan session.id)) selections
    in
    Card.config
        [ sessionToColor categoryMap session
        , Card.attrs
            [ HtmlA.class "session"
            , HtmlA.classList
                [ ( "bookmarked", isBookmarked )
                , ( "planned", isPlanned )
                ]
            ]
        ]
        |> Card.headerH5
            []
            [ Html.span
                [ HtmlA.class "float-right" ]
                [ Html.button
                    [ HtmlE.onClick (BookmarkSession session.id) ]
                    [ renderIcon True <| ifThen isBookmarked "bookmark" "bookmark_border"
                    ]
                , Html.button
                    [ HtmlE.onClick (PlanSession session.id) ]
                    [ renderIcon True <| ifThen isPlanned "star" "star_border"
                    ]
                ]
            , Html.text session.title
            ]
        |> Card.block []
            [ Block.custom <|
                Html.div []
                    [ Html.text session.room
                    , case session.detailsURL of
                        Just url ->
                            Html.button [ HtmlA.class "float-right" ]
                                [ Html.a [ HtmlA.href url, HtmlA.target "_blank" ]
                                    [ renderIcon True "open_in_browser" ]
                                ]

                        Nothing ->
                            Html.text ""
                    ]
            ]


ifThen : Bool -> a -> a -> a
ifThen predicate vT vF =
    if predicate then
        vT

    else
        vF


sessionToColor : CategoryMap -> Schedule.Session -> Card.Option Msg
sessionToColor categoryMap session =
    Dict.get session.category categoryMap
        |> Maybe.andThen (\{ color } -> Just <| Card.attrs [ HtmlA.style "border-color" color, HtmlA.style "background-color" color ])
        |> Maybe.withDefault (Card.attrs [])


renderIcon : Bool -> String -> Html.Html Msg
renderIcon active name =
    Html.i
        [ HtmlA.classList
            [ ( "material-icons", True )
            , ( "inactive", not active )
            ]
        ]
        [ Html.text name ]


renderHacks : Model -> Accordion.Card Msg
renderHacks model =
    Accordion.card
        { id = "hacks"
        , options = []
        , header =
            Accordion.headerH3 [] <| Accordion.toggle [] [ Html.text "Hacks..." ]
        , blocks =
            [ Accordion.block []
                [ Block.custom <|
                    Html.div []
                        [ Html.div [] [ Html.text <| toStorableString model.selections ]
                        , Html.input [ HtmlE.onInput PersistenceHackInput, HtmlA.name "hackPersistence" ] []
                        , Html.button [ HtmlE.onClick PersistenceHackStore ] [ Html.text "Update Persistence (Hack)" ]
                        , Html.div [] [ Html.text "For simple safety, must at least contain 3 characters, otherwise is ignored. To delete use \"DEL\"." ]
                        ]
                ]
            ]
        }
