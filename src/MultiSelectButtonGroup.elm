module MultiSelectButtonGroup exposing
    ( Config
    , Selection
    , State
    , buttons
    , config
    , getSelection
    , getSelectionAsString
    , getSelections
    , initStateFromList
    , initialState
    , isAnySelected
    , isOptionSelected
    , resetSelection
    , view
    )

{-| An extension of Bootstrap ButtonGroups, allowing to build a multi-select
button group, having its state managed.
-}

import Bootstrap.Button as Button
import Bootstrap.ButtonGroup as ButtonGroup
import Bootstrap.Card.Block as Block
import Dict exposing (Dict)
import Html
import Html.Events as HtmlE
import Json.Decode as Json


type Config msg
    = Config
        { id : String
        , toMsg : State -> msg
        , btns : List ( String, String )
        , defaultButtonTitle : String
        }


type State
    = State (Dict String Selection)


type alias Selection =
    List String


type ToggleButton
    = ToggleDefault
    | ToggleOption String


initialState : State
initialState =
    State Dict.empty


initStateFromList : List ( String, List String ) -> State
initStateFromList init =
    State <| Dict.fromList init


config : String -> String -> (State -> msg) -> Config msg
config id defaultButtonTitle toMsg =
    Config
        { id = id
        , toMsg = toMsg
        , btns = []
        , defaultButtonTitle = defaultButtonTitle
        }


buttons : List ( String, String ) -> Config msg -> Config msg
buttons btns (Config conf) =
    Config
        { conf | btns = btns }


view : List (ButtonGroup.Option msg) -> State -> Config msg -> Html.Html msg
view options state ((Config { id, btns, defaultButtonTitle }) as conf) =
    let
        selection =
            getSelection id state
    in
    ButtonGroup.checkboxButtonGroup
        options
        (renderButton defaultButtonTitle ToggleDefault state conf selection
            :: (btns
                    |> List.map (\( k, v ) -> renderButton v (ToggleOption k) state conf selection)
               )
        )


renderButton : String -> ToggleButton -> State -> Config msg -> Selection -> ButtonGroup.CheckboxButtonItem msg
renderButton title toggle state ((Config { id, toMsg }) as conf) selection =
    let
        selected =
            case toggle of
                ToggleDefault ->
                    List.isEmpty selection

                ToggleOption item ->
                    List.member item selection
    in
    ButtonGroup.checkboxButton
        selected
        [ if selected then
            Button.primary

          else
            Button.outlinePrimary
        , Button.onClick (handleToggle toggle state conf)
        ]
        [ Html.text title ]


handleToggle : ToggleButton -> State -> Config msg -> msg
handleToggle toggle (State state) ((Config { id, toMsg }) as conf) =
    let
        selection =
            getSelection id (State state)

        newSelection =
            case toggle of
                ToggleDefault ->
                    []

                ToggleOption option ->
                    if List.member option selection then
                        List.filter ((/=) option) selection

                    else
                        option :: selection
    in
    toMsg <| State <| Dict.insert id newSelection state


resetSelection : String -> State -> State
resetSelection filterGroup (State state) =
    State <| Dict.remove filterGroup state


getSelections : State -> List ( String, List String )
getSelections (State state) =
    Dict.toList state
        |> List.filter (not << List.isEmpty << Tuple.second)


getSelection : String -> State -> Selection
getSelection key (State state) =
    Dict.get key state
        |> Maybe.withDefault []


getSelectionAsString : String -> State -> String
getSelectionAsString key state =
    getSelection key state |> String.join ","


isOptionSelected : String -> String -> State -> Bool
isOptionSelected option id state =
    getSelection id state
        |> List.any ((==) option)


isAnySelected : String -> State -> Bool
isAnySelected id state =
    getSelection id state |> List.isEmpty
